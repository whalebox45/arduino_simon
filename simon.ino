
// Button pin definitions
#define BUTTON_RED 2
#define BUTTON_BLUE 3
#define BUTTON_YELLOW 4
#define BUTTON_GREEN 5

// LED pin definitions
#define LED_RED 8
#define LED_BLUE 9
#define LED_YELLOW 10
#define LED_GREEN 11

// Buzzer definitions
#define BUZZER 7
#define RED_TONE 880
#define BLUE_TONE 1048
#define YELLOW_TONE 1320
#define GREEN_TONE 1568
#define TONE_DURATION 250

//Game Variables
int GAME_SPEED = 300;
int GAME_STATUS = 0;
int const GAME_MAX_SEQ = 50;
int GAME_SEQ[GAME_MAX_SEQ];
int GAME_STEP = 0;
int READ_STEP = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  pinMode(LED_YELLOW, OUTPUT);

  pinMode(BUZZER, OUTPUT);

  pinMode(BUTTON_RED, INPUT_PULLUP);
  pinMode(BUTTON_GREEN, INPUT_PULLUP);
  pinMode(BUTTON_BLUE, INPUT_PULLUP);
  pinMode(BUTTON_YELLOW, INPUT_PULLUP);
}

void loop()
{
  switch (GAME_STATUS)
  {
  case 0:
    standBy();
    break;
  case 1:
    resetGame();
    break;
  case 2:
    playSeq();
    break;
  case 3:
    readSeq();
    break;
  case 4:
    gameOver();
    break;
  }
}

void standBy()
{
  setLEDs(true, false, false, true);
  int button_value = readButtons();
  if (button_value > 0)
  {
    GAME_STATUS = 1;
    setLEDs(false, false, false, false);
    delay(100);
  }
}

void resetGame()
{
  // reset steps
  READ_STEP = 0;
  GAME_STEP = 0;
  // create random sequence
  randomSeed(millis());
  for (int i = 0; i < GAME_MAX_SEQ; i++)
  {
    GAME_SEQ[i] = random(4) + 1;
    Serial.print(GAME_SEQ[i]);
  }
  Serial.println();
  GAME_STATUS = 2;
}

void playSeq()
{
  // play a step of the sequence
  for (int i = 0; i <= GAME_STEP; i++)
  {
    Serial.print("Set LED");
    Serial.println(GAME_SEQ[i]);

    delay(GAME_SPEED * 2);
    setLED(GAME_SEQ[i]);
    playTone(GAME_SEQ[i]);
    delay(GAME_SPEED);
    setLEDs(false, false, false, false);
  }
  GAME_STATUS = 3;
}

void readSeq()
{
  int button_value = readButtons();
  if (button_value > 0)
  {
    // when a button has been buttonPressed
    if (button_value == GAME_SEQ[READ_STEP])
    {
      // correct value
      setLED(button_value);
      playTone(button_value);
      delay(GAME_SPEED);
      setLEDs(false, false, false, false);

      if (READ_STEP == GAME_STEP)
      {
        // reset read step
        READ_STEP = 0;
        // go to next game step
        GAME_STEP++;
        // go to play sequence mode
        GAME_STATUS = 2;
        Serial.println("Go to next step");
        setLEDs(true, true, true, true);
        delay(GAME_SPEED);
        setLEDs(false, false, false, false);
        // Speed up
        GAME_SPEED -= 15;
      }

      else
      {
        READ_STEP++;
      }
      delay(10);
    }
    else
    {
      // wrong value, go to game over mode
      GAME_STATUS = 4;
      Serial.println("Game Over!");
    }
  }

  delay(10);
}
void gameOver()
{

  setLEDs(true, true, true, true);
  // play sad trombone
  tone(BUZZER, 980, TONE_DURATION);
  delay(TONE_DURATION);
  tone(BUZZER, 930, TONE_DURATION);
  delay(TONE_DURATION);
  tone(BUZZER, 870, TONE_DURATION);
  delay(TONE_DURATION);
  setLEDs(false, false, false, false);
  delay(250);
  GAME_SPEED = 300;
  GAME_STATUS = 0;
}

void playTone(int id)
{
  switch (id)
  {
  case 0:
    noTone(BUZZER);
    break;
  case 1:
    tone(BUZZER, RED_TONE, TONE_DURATION);
    break;
  case 2:
    tone(BUZZER, BLUE_TONE, TONE_DURATION);
    break;
  case 3:
    tone(BUZZER, YELLOW_TONE, TONE_DURATION);
    break;
  case 4:
    tone(BUZZER, GREEN_TONE, TONE_DURATION);
    break;
  }
}

void setLED(int id)
{

  switch (id)
  {
  case 0:
    setLEDs(false, false, false, false);
    break;
  case 1:
    setLEDs(true, false, false, false);
    break;
  case 2:
    setLEDs(false, true, false, false);
    break;
  case 3:
    setLEDs(false, false, true, false);
    break;
  case 4:
    setLEDs(false, false, false, true);
    break;
  }
}

void setLEDs(bool red, bool blue, bool yellow, bool green)
{
  if (red)
    digitalWrite(LED_RED, HIGH);
  else
    digitalWrite(LED_RED, LOW);
  if (green)
    digitalWrite(LED_GREEN, HIGH);
  else
    digitalWrite(LED_GREEN, LOW);
  if (blue)
    digitalWrite(LED_BLUE, HIGH);
  else
    digitalWrite(LED_BLUE, LOW);
  if (yellow)
    digitalWrite(LED_YELLOW, HIGH);
  else
    digitalWrite(LED_YELLOW, LOW);
}

int readButtons(void)
{
  if (digitalRead(BUTTON_RED) == 0)
    return 1;
  else if (digitalRead(BUTTON_BLUE) == 0)
    return 2;
  else if (digitalRead(BUTTON_YELLOW) == 0)
    return 3;
  else if (digitalRead(BUTTON_GREEN) == 0)
    return 4;
  return 0;
}

bool buttonPressed(int pin)
{
  return !digitalRead(pin);
}
